Implementación de oracle y symfony (Ubuntu 20.04)
===================


A continuación se detallan los pasos de instalación, configuración y resolución de algunos errores que pueden surgir

----------
**Imagen Docker de Oracle**

Imagen utilizada:
https://hub.docker.com/_/oracle-database-enterprise-edition/

**Descargar la imagen y levantar el servicio**

```
docker-compose up -d
```
----------

**Instalacion de Java**

Necesitaremos instalar Java (JRE y del JDK predeterminados). Para la versión de SqlDeveloper 20.2 utilizamos Java 11

Primero, actualice su lista de paquetes existente:
```
sudo apt update
```

El siguiente comando instala JRE

```

sudo apt install default-jre
```
Verifique la instalación con lo siguiente:

```
java -version
```
Deberá obtener una salida similar a:

>openjdk version "11.0.7" 2020-04-14
>OpenJDK Runtime Environment (build 11.0.7+10-post-Ubuntu-3ubuntu1)
>OpenJDK 64-Bit Server VM (build 11.0.7+10-post-Ubuntu-3ubuntu1, mixed mode, sharing)

Instalación de JRE

```
sudo apt install default-jdk
```
Verificar instalacion

```
javac -version
```

Salida:
```
javac 11.0.7
```
--------
**SQL*Plus **

SQL*Plus es un programa de línea de comandos de Oracle que puede ejecutar comandos SQL y PL/SQL de forma interactiva o mediante un script.

Al trabajar con un contenedor docker es necesario trabajar dentro del bash de ese contenedor

Para ingresar primero listamos los contenedores activos

```
docker ps
```
Copiamos el ID de nuestro contenedor y corremos

```
docker exec -it <Oracle-DB> bash -c "source /home/oracle/.bashrc; sqlplus"

```
esto abre una terminal dentro del contenedor y ejecutamos:
```
sqlplus
```
Credenciales de acceso
```
user:system
pass:Oradoc_db1
```

Oracle trabaja generando para un usuario su propia base de datos, se puede crear desde terminal a traves de sqlplus. Tambien se puede generar a traves de Sql Developer detallado mas adelante

```
Antes de Crear un usuario vamos a ejecutar el siguiente comando, para que nos permita crear un usuario sin tener que incluir caracteres especiales en nombre de usuario.
```
alter session set "_Oracle_SCRIPT"=true;

```
Luego procedemos a crear el usario, reemplzanado "nombreDeUsuario" y "contraseña" por nuestros datos.

```
create user "nombreDeUsuario" identified by "contraseña";
```

Al ejecutar la instrucción nos habrá creado el usuario. Ahora habrá que otorgarle ciertos permisos.

```
grant connect , resource to “nombre”
```
Y con eso tenemos creado nuestor usuario (esquema de base de datos).

Para borrar un usuario realizamos la siguiente instruccion:

```
Drop user “usuario”;
```

En caso de que tenga tablas y esten relacionadas colocamos:
```
Drop user “usuario” cascade;
```

> **Nota:**

> Nombre = “Nombre de usuario o esquema que queremos crear”
> Users= “Tabla users, puede ser system, users, etc”

Se sugiere trabajar con un gestor visual de BD

Ahora veremos como instalar SqlDeveloper, una herramienta para bases de datos Oracle

--------
**SqlDeveloper**

Necesitamos descargar el archivo desde la pagina oficial de Oracle. Se adjunta el siguiente link pero se sugiere revisar las versiones antes de proceder con la instalación. (Se debe descargar el archivo generado para Linux RPM)
https://www.oracle.com/tools/downloads/sqldev-downloads.html

Una vez descargado el archivo vamos a convertir el paquete  Red Hat a un paquete de Debian
```
sudo alien --scripts -d sqldeveloper-20.2.0.175.1842-20.2.0-175.1842.noarch.rpm
```
> **Nota:**

> Reemplazar el nombre del archivo por el nombre del archivo descargado
> Este proceso puede demorar algunos minutos

Se genera un archivo con el nombre de sqldeveloper_20.2.0-176.1842_all.deb. Para ejecutar este archivo, ejecute la siguiente instrucción:
```
sudo dpkg --install sqldeveloper_20.2.0.176.1842_all.deb
```

Ejecutamos SQL Developer desde el terminal.
```
sudo /opt/sqldeveloper/sqldeveloper.sh
```
> **Nota:**
>Si se muestra un error de compatibilidad de java, tendrás que modificar el archivo product.cnf. Nota: cambiar "su usuario" en el siguiente comando.
sudo gedit /home/"su usuario"/.sqldeveloper/20.2.0/product.conf
>Ahora modificamos en la parte que dice # SetJavaHome /path/jdk, le quitamos el signo # y colocamos el path de java, en mi caso:
SetJavaHome /usr/lib/jvm/java-1.8.0-openjdk-amd64

Enlace con el paso a paso de una creación de un usuario con SqlDeveloper
https://oraxedatabase.blogspot.com/2019/03/crear-usuarios-asignar-y-revocar-roles.html

--------
**configurar .env **

En nuestro archivo .env
```
DATABASE_URL="oci8://USER:PASS@HOST:PUERTO/SID" 
(Para conocer el service que utiliza el SID, debemos ejecutar el siguiente comando en SQLPLUS o SQLDeveloper: select value from v$parameter where name='service_names')
```

Actualizamos nuestro .env.local
```
composer dump-env dev
```
A continuacion podemos crear una entidad o generar una migracion ya existente
```
crear entidad
php bin/console make:entity {nombre entidad}
```
```
correr migraciones
php bin/console doctrine:schema:update --force
```

Podemos encontrarnos con los siguientes errores:

"An exception occurred in driver: Database error occurred but no error information was retrieved from the driver. "

Se sugiere revisar el archivo services.yaml

>parameters:
        database_driver: oci8
        database_host: HOST A USAR
        database_port: 'PUERTO’ (oracle trabaja con el 1521 por default - debe colocarse	   entre comillas simples)
        database_name: DB_NAME
        database_user: USER_NAME
        database_password: PASS
services:
    # default configuration for services in *this* file
    _defaults:
        autowire: true      # Automatically injects dependencies in your services.
        autoconfigure: true # Automatically registers your services as commands, event subscribers, etc.
    oracle.listener:
        class: Doctrine\DBAL\Event\Listeners\OracleSessionInit
        tags:
            - { name: doctrine.event_listener, event: postConnect }

Otro error con el que podemos encontrarnos es:

"Unknown database type number requested, Doctrine\DBAL\Platforms\OraclePlatform may not support it"

Para este error debemos mapear manual el tipo de dato en el archivo doctrine.yaml de la siguiente manera

> dbal:
        url: '%env(resolve:DATABASE_URL)%'
        driver: 'oci8'
        server_version: ~
        charset: utf8mb4
        service: true
        mapping_types:
           **aq$_subscribers: string**